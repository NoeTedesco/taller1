package com.taller.sms.primerTaller;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import junit.framework.Assert;

public class Casos {

	private WebDriver driver;
	private WebDriverWait wait;
	private WebElement toasMessage;

	@BeforeSuite
	public void init() {
		System.setProperty("webdriver.chrome.driver", "C:\\ChromeDriver\\chromedriver.exe");
		driver = new ChromeDriver();
		wait = new WebDriverWait(driver, 10);
		driver.get("https://tsegundaopinion.osde.com.ar/prestadores/signin");
	}
	
	@Test (priority = 0)
	public void iniciarSesion() {
		WebElement cmpUsuario = driver.findElement(By.name("user"));
		wait.until(ExpectedConditions.elementToBeClickable(cmpUsuario));
		Assert.assertTrue(cmpUsuario.isDisplayed());
		cmpUsuario.click();
		cmpUsuario.sendKeys("ntedesco@sms-latam.com");
		WebElement cmpPass = driver.findElement(By.name("password"));
		Assert.assertTrue(cmpPass.isDisplayed());
		cmpPass.click();
		cmpPass.sendKeys("Smotest1!");
		WebElement btnIngresar = driver.findElement(By.id("prestadores-signin-btn-login"));
		Assert.assertTrue(btnIngresar.isDisplayed());
		btnIngresar.click();
		wait.until(ExpectedConditions.urlContains("/prestadores/home"));
		toasMessage = wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.className("toast-message"))));
		Assert.assertEquals(toasMessage.getText(),"Bienvenido!");
	}
	
	
	@Test (priority = 1)
	public void VisualizarElementosHome() {
		WebElement btnInterconsulta = driver.findElement(By.xpath("//*[@href='/prestadores/enviar-interconsulta-vc']"));
		wait.until(ExpectedConditions.elementToBeClickable(btnInterconsulta));
		Assert.assertTrue(btnInterconsulta.isDisplayed());
		wait.until(ExpectedConditions.stalenessOf(driver.findElement(By.className("toast-message"))));
		WebElement nombreUsuario = wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//*[@id='navbar']/ul/li[1]/a"))));
		Assert.assertEquals(nombreUsuario.getText(),"Bienvenido, Usuario Noelia Tedesco");
		WebElement btnCambiarCon = driver.findElement(By.xpath("//a[@href='/prestadores/mi-perfil/cambiar-password']"));
		Assert.assertTrue(btnCambiarCon.isDisplayed());
		WebElement btnSalir = driver.findElement(By.id("btSalir"));
		Assert.assertTrue(btnSalir.isDisplayed());

		
	}
	
	
	@Test (priority = 3)
	public void GenerarInvitacion() throws InterruptedException {
			WebElement btnInterconsulta = driver.findElement(By.xpath("//*[@href='/prestadores/enviar-interconsulta-vc']"));
			btnInterconsulta.click();
			wait.until(ExpectedConditions.urlContains("prestadores/enviar-interconsulta-vc"));
			WebElement btnEnviarInv = driver.findElement(By.xpath("//*[@id=\"main\"]/div/div/div/div/div[1]/button"));
			wait.until(ExpectedConditions.elementToBeClickable(btnEnviarInv));
			Assert.assertTrue(btnEnviarInv.isDisplayed());
			btnEnviarInv.click();
			WebElement formInvitacion = driver.findElement(By.xpath("//*[@id=\"main\"]/div/div/div/div/div[2]/div"));
			wait.until(ExpectedConditions.visibilityOf(formInvitacion));
			Assert.assertTrue(formInvitacion.isDisplayed());
			WebElement cmpNumero = driver.findElement(By.name("valorIdentificacion"));
			cmpNumero.sendKeys("55555555555");
			WebElement cmpApellido = driver.findElement(By.id("apellido"));
			cmpApellido.sendKeys("Lopez");
			WebElement cmpNombre = driver.findElement(By.id("nombre"));
			cmpNombre.sendKeys("Marta");
			WebElement cmpFechaNacimiento = driver.findElement(By.id("fechaNacimiento"));
			cmpFechaNacimiento.click();
			cmpFechaNacimiento.sendKeys("11012001");
			WebElement cmpApellidoMed = driver.findElement(By.id("apellidoSolicitante"));
			cmpApellidoMed.sendKeys("Gomez");
			WebElement cmpNombreoMed = driver.findElement(By.id("nombreSolicitante"));
			cmpNombreoMed.sendKeys("Marco");
			WebElement tipoMatricula = driver.findElement(By.id("tipoMatricula"));
			Select tipoM = new Select (tipoMatricula);
			tipoM.selectByValue("3");
			WebElement numMatricula = driver.findElement(By.id("nroMatricula"));
			numMatricula.sendKeys("2131231231");
			WebElement cmpEmail = driver.findElement(By.id("email"));
			cmpEmail.sendKeys("ntedesco@sms-latam.com");
			WebElement cmpCelular = driver.findElement(By.name("phone"));
			cmpCelular.sendKeys("1130170508");
			WebElement selEspecialidad = driver.findElement(By.id("especialidad"));
			Select selEsp = new Select(selEspecialidad);
			selEsp.selectByVisibleText("Clínica Adultos");
			WebElement selProf = driver.findElement(By.id("profesional"));
			Select selP = new Select(selProf);
			wait.until(ExpectedConditions.attributeContains(selProf, "textContent", "Smotest Cinco"));
			selP.selectByVisibleText("Smotest Cinco");
			WebElement motvInt =driver.findElement(By.name("detalle"));
			motvInt.sendKeys("Texto prueba");
			
			WebElement btnEnviar = driver.findElement(By.xpath("//*[@id=\"enviarInvitacionForm\"]/div/footer/div/div/div/div[2]/button"));
			btnEnviar.click();
			Thread.sleep(3000);
	}
	
	
	@AfterSuite
	public void cerrarDrive() {
		driver.close();
		driver.quit();
	}


}
